<?php

/**
 * @file
 * Module allows export block as a widget for embedding to a 3-rd party site.
 */

/**
 * Implements hook_menu().
 */
function block_export_widget_menu() {
  $items['block/export/%/%'] = array(
    'title' => 'Block export widget',
    'page callback' => 'block_export_widget_output',
    'page arguments' => array(2, 3),
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
    'delivery callback' => 'block_export_widget_deliver_html_output',
  );

  $items['admin/config/services/block_export'] = array(
    'title' => 'Block export widget',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('block_export_widget_admin_configure_form'),
    'access arguments' => array('administer blocks'),
    'file' => 'block_export_widget.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function block_export_widget_theme() {
  return array(
    'block_export_widget_admin_configure_form' => array(
      'template' => 'block-export-widget-admin-configure-form',
      'file' => 'block_export_widget.admin.inc',
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_help().
 */
function block_export_widget_help($path, $arg) {
  switch ($path) {
    case 'admin/config/services/block_export':
      $output = '<p>' . t('This page provides an interface for enabling export for the blocks, and for controlling the export formats of blocks. Remember that your changes will not be saved until you click the <em>Save configuration</em> button at the bottom of the page.') . '</p>';
      $output .= '<p>' . t('Click the <em>export</em> link next to the block to see block export widget. <em>Please notice</em> that for the 3rd party site block will be exported using anonymous user access permissions.') . '</p>';
      return $output;
  }
}

/**
 * Returns export configuration for the specific block.
 *
 * @param string $module
 *   Module that provides block.
 * @param string $delta
 *   Block delta.
 *
 * @return array|null
 *   Array of config or null.
 */
function block_export_widget_get_config($module, $delta) {
  $config = variable_get('block_export_widget', array());
  $key = $module . '_' . $delta;

  return isset($config[$key]) ? $config[$key] : NULL;
}

/**
 * Returns list of blocks which have been excluded from export.
 *
 * Some blocks depend great on Drupal context and page or have
 * content only for authenticated users - so it makes no sense
 * to export them to the 3rd party site.
 *
 * @return array
 *   Array of excluded blocks.
 */
function block_export_widget_excluded_blocks() {
  $excluded_blocks = &drupal_static(__FUNCTION__);

  if (!isset($excluded_blocks)) {
    $excluded_blocks = array(
      'system_main', 'system_help',
      'devel_execute_php', 'devel_switch_user',
      'boxes_boxes_add__simple',
      'context_ui_editor', 'context_ui_devel',
      'diff_inline',
    );

    drupal_alter('block_export_widget_excluded_blocks', $excluded_blocks);
  }

  return $excluded_blocks;
}

/**
 * Returns list of export formats for specific block.
 *
 * @param string $module
 *   Module that provides block.
 * @param string $delta
 *   Block delta.
 *
 * @return array
 *   Array of available formats.
 */
function block_export_widget_formats($module, $delta) {
  $default_formats = &drupal_static(__FUNCTION__);

  if (!isset($default_formats)) {
    $default_formats = array(
      t('Default block') => array(
        'default' => t('Default block widget'),
        'default|no_title' => t('Default block widget without title'),
      ),
    );
  }

  $formats = $default_formats;

  drupal_alter('block_export_widget_formats', $formats, $module, $delta);

  return $formats;
}

/**
 * Implements hook_block_expot_widget_formats().
 *
 * Adds custom formats for simple text blocks and boxes.
 */
function block_export_widget_block_export_widget_formats_alter(array &$formats, $module, $delta) {
  switch ($module) {
    case 'block':
      $formats[t('Simple text block')] = array(
        'block_content' => t('Simple block content only'),
      );
      break;

    case 'boxes':
      $formats[t('Simple text box')] = array(
        'boxes_content' => t('Simple box content only'),
      );
  }
}

/**
 * Menu callback for block/export/%/%.
 *
 * Outputs rendered block widget or 404 error if block not found
 * or export is not enabled.
 *
 * @param string $module
 *   Module that provides block.
 * @param string $delta
 *   Block delta.
 */
function block_export_widget_output($module, $delta) {
  $block_export = block_export_widget_get_config($module, $delta);
  $block_render = NULL;

  if ($block_export && $block_export['export']) {
    $format_info = explode('|', $block_export['format']);
    $format_name = $format_info[0];
    $format_args = explode(',', isset($format_info[1]) ? $format_info[1] : '');

    $block_render = module_invoke_all('block_export_widget_format_' . $format_name, $block_export, $format_args);
  }

  if ($block_render) {
    return $block_render;
  }
  else {
    return MENU_NOT_FOUND;
  }
}

/**
 * Packages and sends the result of a export callback to the browser as HTML.
 *
 * This is lightweight and fast version of drupal_deliver_html_page().
 * Also MENU_SITE_OFFLINE just returns 403 error.
 *
 * @param mixed $page_callback_result
 *   The result of a page callback. Can be one of:
 *   - NULL: to indicate no content.
 *   - An integer menu status constant: to indicate an error condition.
 *   - A string of HTML content.
 *   - A renderable array of content.
 *
 * @see drupal_deliver_html_page()
 */
function block_export_widget_deliver_html_output($page_callback_result) {
  // Emit the correct charset HTTP header, but not if the page callback
  // result is NULL, since that likely indicates that it printed something
  // in which case, no further headers may be sent, and not if code running
  // for this page request has already set the content type header.
  if (isset($page_callback_result) && is_null(drupal_get_http_header('Content-Type'))) {
    drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
  }

  // Send appropriate HTTP-Header for browsers and search engines.
  global $language;
  drupal_add_http_header('Content-Language', $language->language);

  // Menu status constants are integers; page content is a string or array.
  if (is_int($page_callback_result)) {
    switch ($page_callback_result) {
      case MENU_NOT_FOUND:
        // Print a 404 page.
        drupal_add_http_header('Status', '404 Not Found');
        break;

      case MENU_ACCESS_DENIED:
      case MENU_SITE_OFFLINE:
        // Print a 403 page.
        drupal_add_http_header('Status', '403 Forbidden');
        break;
    }
  }
  elseif (isset($page_callback_result)) {
    // Print anything besides a menu constant, assuming it's not NULL or
    // undefined.
    print drupal_render($page_callback_result);
  }
}

/**
 * Implements hook_block_export_widget_format_FORMAT_NAME().
 */
function block_export_widget_block_export_widget_format_default(array $block_export, array $args) {
  $block = block_load($block_export['module'], $block_export['delta']);

  if ($block) {
    $block_render = _block_get_renderable_array(_block_render_blocks(array($block)));

    foreach (element_children($block_render) as $element) {
      $block_render[$element]['#block']->region = 'block_export_widget';
      if (in_array('no_title', $args)) {
        $block_render[$element]['#block']->subject = '';
      }
    }

    return $block_render;
  }
  else {
    return NULL;
  }
}

/**
 * Implements hook_block_export_widget_format_FORMAT_NAME().
 */
function block_export_widget_block_export_widget_format_block_content(array $block_export, array $args) {
  $block = block_load($block_export['module'], $block_export['delta']);

  if ($block) {
    $block_render = _block_render_blocks(array($block));
    return $block_render[$block_export['module'] . '_' . $block_export['delta']]->content;
  }
  else {
    return NULL;
  }
}

/**
 * Implements hook_block_export_widget_format_FORMAT_NAME().
 */
function block_export_widget_block_export_widget_format_boxes_content($block_export, $args) {
  $box = boxes_box_load($block_export['delta']);

  if ($box) {
    $box_render = $box->render();
    return array(
      '#markup' => $box_render['content'],
    );
  }
  else {
    return NULL;
  }
}
